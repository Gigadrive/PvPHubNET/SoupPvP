package tk.Zeryther.pvp_hub.spvp.menus;

import net.pvp_hub.core.api.PlayerUtilities;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import tk.Zeryther.pvp_hub.spvp.utilities.Util;

public class KitList {

	private static Inventory kl;
	
	public static void openFor(Player p){
		initList(p);
	    p.openInventory(kl);
	}
	
	public static void initList(Player p){
		kl = Bukkit.createInventory(null, 18, "�e�lSoupPvP �r�7- �3Kits");
		kl.setItem(0, Util.namedItem(Material.STAINED_GLASS_PANE, " ", new String[] { " " }));
		kl.setItem(1, Util.namedItem(Material.IRON_SWORD, "�a�lAssault", new String[] { "�7Kit: �a�lAssault" }));
		kl.setItem(2, Util.namedItem(Material.STAINED_GLASS_PANE, " ", new String[] { " " }));
		kl.setItem(3, Util.namedItem(Material.BOW, "�c�lArcher", new String[] { "�7Kit: �c�lArcher" }));
		kl.setItem(4, Util.namedItem(Material.STAINED_GLASS_PANE, " ", new String[] { " " }));
		kl.setItem(5, Util.namedItem(Material.POTION, "�e�lChemist", new String[] { "�7Kit: �e�lChemist" }));
		kl.setItem(6, Util.namedItem(Material.STAINED_GLASS_PANE, " ", new String[] { " " }));
		kl.setItem(7, Util.namedItem(Material.APPLE, "�2�lAssist", new String[] { "�7Kit: �2�lAssist" }));
		kl.setItem(8, Util.namedItem(Material.STAINED_GLASS_PANE, " ", new String[] { " " }));
		kl.setItem(9, Util.namedItem(Material.STAINED_GLASS_PANE, " ", new String[] { " " }));
		kl.setItem(10, Util.namedItem(Material.SPECKLED_MELON, "�d�lAllround", new String[] { "�7Kit: �d�lAllround" }));
		kl.setItem(11, Util.namedItem(Material.STAINED_GLASS_PANE, " ", new String[] { " " }));
		try {
			if(PlayerUtilities.hasBoughtShopItem(p, 18)){
				kl.setItem(12, Util.namedItem(Material.DIAMOND_SWORD, "�3�lSurvivor", new String[] { "�7Kit: �3�lSurvivor" }));
			} else {
				kl.setItem(12, Util.namedItem(Material.CHEST, "�8???", new String[] { "�7Kaufe diese Klasse im Lobby-Shop" }));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		kl.setItem(13, Util.namedItem(Material.STAINED_GLASS_PANE, " ", new String[] { " " }));
		try {
			if(PlayerUtilities.hasBoughtShopItem(p, 19)){
				kl.setItem(14, Util.namedItem(Material.DIAMOND_CHESTPLATE, "�b�lHeavy", new String[] { "�7Kit: �b�lHeavy" }));
			} else {
				kl.setItem(14, Util.namedItem(Material.CHEST, "�8???", new String[] { "�7Kaufe diese Klasse im Lobby-Shop" }));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		kl.setItem(15, Util.namedItem(Material.STAINED_GLASS_PANE, " ", new String[] { " " }));
		try {
			if(PlayerUtilities.hasBoughtShopItem(p, 17)){
				kl.setItem(16, Util.namedItem(Material.STICK, "�5�lWizard", new String[] { "�7Kit: �5�lWizard" }));
			} else {
				kl.setItem(16, Util.namedItem(Material.CHEST, "�8???", new String[] { "�7Kaufe diese Klasse im Lobby-Shop" }));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		kl.setItem(17, Util.namedItem(Material.STAINED_GLASS_PANE, " ", new String[] { " " }));
	}
	
}
