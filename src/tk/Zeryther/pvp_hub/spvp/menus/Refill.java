package tk.Zeryther.pvp_hub.spvp.menus;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import tk.Zeryther.pvp_hub.spvp.utilities.Util;

public class Refill {

	private static Inventory rf;
	
	public static void openFor(Player p){
		initList();
	    p.openInventory(rf);
	}
	
	public static void initList(){
		
		ItemStack mushroomSoup = new ItemStack(Material.MUSHROOM_SOUP, 1);
		ItemMeta mushMeta = mushroomSoup.getItemMeta();
		mushMeta.setDisplayName("�9�lHealing-Soup");
		mushroomSoup.setItemMeta(mushMeta);
		mushroomSoup.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
		
		if(rf == null){
			rf = Bukkit.createInventory(null, 45, "�e�lSoupPvP �r�7- �3REFILL");
			for(int i = 1; i <= 46; i++) {
				rf.addItem(mushroomSoup);
			}
		}
	}
	
}
