package tk.Zeryther.pvp_hub.spvp.main;

public class Vars {
	// Chat Prefix
	public static final String prefix = "�6[�eSoupPvP�6] ";
		
	// Chat Prefix
	public static final String prefix_pvphub = "�4[PvP-Hub] �6";
	
	// 'No-Permssion' Message
	public static final String nopermission = prefix + "�cDu hast keine Rechte f�r diese Aktion!";
	
	// 'Coming soon' Message
	public static final String soon = prefix + "Dieses Feature ist derzeit noch nicht implementiert!";
	
	// Console Logger Prefix
	public static final String l_prefix = "[LOGGER|SPVP] ";
}
