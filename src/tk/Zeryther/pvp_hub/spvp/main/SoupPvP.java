package tk.Zeryther.pvp_hub.spvp.main;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.GameState;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import tk.Zeryther.pvp_hub.spvp.listener.AsyncPlayerChatListener;
import tk.Zeryther.pvp_hub.spvp.listener.AsyncPlayerPreLoginListener;
import tk.Zeryther.pvp_hub.spvp.listener.FoodLevelChangeListener;
import tk.Zeryther.pvp_hub.spvp.listener.InventoryClickListener;
import tk.Zeryther.pvp_hub.spvp.listener.PlayerDeathListener;
import tk.Zeryther.pvp_hub.spvp.listener.PlayerDisconnectListener;
import tk.Zeryther.pvp_hub.spvp.listener.PlayerDropListener;
import tk.Zeryther.pvp_hub.spvp.listener.PlayerInteractEntityListener;
import tk.Zeryther.pvp_hub.spvp.listener.PlayerInteractListener;
import tk.Zeryther.pvp_hub.spvp.listener.PlayerJoinListener;
import tk.Zeryther.pvp_hub.spvp.listener.PlayerRespawnListener;
import tk.Zeryther.pvp_hub.spvp.listener.SecondInventoryClickListener;
import tk.Zeryther.pvp_hub.spvp.listener.ServerPingListener;
import tk.Zeryther.pvp_hub.spvp.listener.WeatherChangeListener;
import tk.theakio.sql.HandlerStorage;
import tk.theakio.sql.MySQLData;
import tk.theakio.sql.QueryResult;
import tk.theakio.sql.SQLHandler;

@SuppressWarnings("unused")
public class SoupPvP extends JavaPlugin implements Listener {
	
	// Initialize YamlConfigs
	public File file = new File("plugins/SoupPvP", "config.yml");
	public FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	
	private QueryResult qr = null;
	
	public static int PID = 0;
	
	// Initialize Internal Array-Lists
	public ArrayList<String> is_ingame = new ArrayList<String>();
	public ArrayList<String> used_fireball = new ArrayList<>();
	public ArrayList<String> used_arrow = new ArrayList<>();
	
	// Class Array-Lists
	public ArrayList<Player> class_assault = new ArrayList<Player>();
	public ArrayList<Player> class_archer = new ArrayList<Player>();
	public ArrayList<Player> class_chemist = new ArrayList<Player>();
	public ArrayList<Player> class_assist = new ArrayList<Player>();
	public ArrayList<Player> class_allround = new ArrayList<Player>();
	public ArrayList<Player> class_wizard = new ArrayList<Player>();
	public ArrayList<Player> class_survivor = new ArrayList<Player>();
	public ArrayList<Player> class_heavy = new ArrayList<Player>();
	
	public static HashMap<Player, Integer> killStreak = new HashMap<Player, Integer>();
	
	// Initialize Internal Strings
	public String main = "main";
	
	public Scoreboard board = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
	public Objective main_objective = board.registerNewObjective("side", "dummy");
	
	@SuppressWarnings("deprecation")
	public Score points = main_objective.getScore(Bukkit.getOfflinePlayer("�7Punkte:"));
	@SuppressWarnings("deprecation")
	public Score kills = main_objective.getScore(Bukkit.getOfflinePlayer("�7Kills:"));
	@SuppressWarnings("deprecation")
	public Score deaths = main_objective.getScore(Bukkit.getOfflinePlayer("�7Tode:"));

	public static int PID_ = 0;
	
	public void onEnable(){
		System.out.println("[SPVP 2.0] ENABLED!");
		
		// Register Main Listener (May be empty)
		getServer().getPluginManager().registerEvents(this, this);
		
		PvPHubCore.setGameState(GameState.ALWAYS_JOIN);
		
		// Register Additional Events
		getServer().getPluginManager().registerEvents(new PlayerJoinListener(this), this);
		getServer().getPluginManager().registerEvents(new PlayerDisconnectListener(this), this);
		getServer().getPluginManager().registerEvents(new PlayerDropListener(this), this);
		getServer().getPluginManager().registerEvents(new PlayerInteractListener(this), this);
		getServer().getPluginManager().registerEvents(new WeatherChangeListener(this), this);
		getServer().getPluginManager().registerEvents(new InventoryClickListener(this), this);
		getServer().getPluginManager().registerEvents(new SecondInventoryClickListener(this), this);
		getServer().getPluginManager().registerEvents(new FoodLevelChangeListener(this), this);
		getServer().getPluginManager().registerEvents(new PlayerDeathListener(this), this);
		getServer().getPluginManager().registerEvents(new PlayerRespawnListener(this), this);
		getServer().getPluginManager().registerEvents(new PlayerInteractEntityListener(this), this);
		getServer().getPluginManager().registerEvents(new AsyncPlayerPreLoginListener(this), this);
		getServer().getPluginManager().registerEvents(new AsyncPlayerChatListener(this), this);
		//getServer().getPluginManager().registerEvents(new ServerPingListener(this), this);
		
		this.main_objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		this.main_objective.setDisplayName("�e�lSoupPvP");
		
		SQLHandler Handler = HandlerStorage.addHandler(main, new SQLHandler());
		Handler.setMySQLOptions(MySQLData.mysql_u_host, MySQLData.mysql_u_port, MySQLData.mysql_u_user, MySQLData.mysql_u_pass, MySQLData.mysql_u_database);
		try {
			Handler.openConnection();
		} catch (Exception e) {
			System.err.println("[MySQL API von Akio Zeugs] Putt Putt!");
		}
		
		PID_ = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {

			public void run() {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "remove items 3000");
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "remove arrows 3000");
				
				for(Player all : Bukkit.getOnlinePlayers()){
					if(all.isOp()){
						all.sendMessage(PluginMeta.sn_prefix + "�eSoupPvP Logger �8| �7Alle Items wurden entfernt.");
						all.sendMessage(PluginMeta.sn_prefix + "�eSoupPvP Logger �8| �7Alle Pfeile wurden entfernt.");
					}
				}
			}
		}
				
				
				
				, 60L, 2000L);
		
		//Handler.executeQuery();
		
		String currentDir = System.getProperty("config.yml");
		File Dir = new File(currentDir + "/plugins/SoupPvP/config.yml");
		if(!Dir.exists()){
			saveDefaultConfig();
		}
		
		getConfig().addDefault("SPVP.MySQL.host", "localhost");
		getConfig().addDefault("SPVP.MySQL.user", "root");
		getConfig().addDefault("SPVP.MySQL.password", "password");
		getConfig().addDefault("SPVP.MySQL.port", 3306);
		getConfig().addDefault("SPVP.MySQL.database", "SoupPvP");
		saveConfig();
	}
	
	public int getPoints(String p) throws Exception {
		int points = 100;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM spvp_stats WHERE mojangid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			points = qr.rs.getInt("points");
		}
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
		
		return points;
	}
	
	public String getClassChatFormat(Player p){
		if(this.class_allround.contains(p)){
			return "�dAllround";
		} else if(this.class_archer.contains(p)){
			return "�cArcher";
		} else if(this.class_assault.contains(p)){
			return "�aAssault";
		} else if(this.class_assist.contains(p)){
			return "�2Assist";
		} else if(this.class_chemist.contains(p)){
			return "�eChemist";
		} else if(this.class_heavy.contains(p)){
			return "�bHeavy";
		} else if(this.class_survivor.contains(p)){
			return "�3Survivor";
		} else if(this.class_wizard.contains(p)){
			return "�5Wizard";
		} else {
			return "�7Lobby";
		}
	}
	
	public void removeFromArrayLists(Player p){
		if(this.class_allround.contains(p)){
			this.class_allround.remove(p);
		} else if(this.class_archer.contains(p)){
			this.class_archer.remove(p);
		} else if(this.class_assault.contains(p)){
			this.class_assault.remove(p);
		} else if(this.class_assist.contains(p)){
			this.class_assist.remove(p);
		} else if(this.class_chemist.contains(p)){
			this.class_chemist.remove(p);
		} else if(this.class_heavy.contains(p)){
			this.class_heavy.remove(p);
		} else if(this.class_heavy.contains(p)){
			this.class_heavy.remove(p);
		} else if(this.class_survivor.contains(p)){
			this.class_survivor.remove(p);
		} else if(this.class_wizard.contains(p)){
			this.class_wizard.remove(p);
		} else if(this.is_ingame.contains(p.getName())){
			this.class_allround.remove(p.getName());
		}
	}
	
	public void addDeaths(String p) throws Exception {
		int deaths = this.getDeaths(p);
		
		deaths++; // +1
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `spvp_stats` SET `deaths` = " + deaths + " WHERE mojangid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public void addKills(String p) throws Exception {
		int kills = this.getKills(p);
		
		kills++; // +1
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `spvp_stats` SET `kills` = " + kills + " WHERE mojangid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public void addPoints(String p) throws Exception {
		int points = this.getPoints(p);
		
		points++; // +1
		points++; // +2
		points++; // +3
		points++; // +4
		points++; // +5
		
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `spvp_stats` SET `points` = " + points + " WHERE mojangid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e){
		  	e.printStackTrace();
		}
	}
	
	public void isInDatabase(String p) throws Exception {
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM spvp_stats WHERE mojangid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();

		if (qr.rs.getRow() == 0)
	      {
	        System.err.println("User not Found!");
	        HandlerStorage.getHandler("main").execute("INSERT INTO `spvp_stats` (`id` ,`mojangid` ,`lastname` ,`kills` ,`deaths` ,`points` ,`exist_count`)VALUES (NULL , '" + PlayerUtilities.getUUID(p) + "', '" + p + "', '0', '0', '100', 'y');");
	      }
		
		HandlerStorage.getHandler("main").closeRecources(qr.rs, qr.st);
	}
	
	public void removePoints(String p) throws Exception {
		int points = this.getPoints(p);
		
		points--; // -1
		points--; // -2
		points--; // -3
		points--; // -4
		points--; // -5
		
		try {
		     HandlerStorage.getHandler("main").execute("UPDATE `spvp_stats` SET `points` = " + points + " WHERE mojangid='" + PlayerUtilities.getUUID(p) + "'");
		} catch (Exception e) {
		  	e.printStackTrace();
		}
	}
	
	public int getKills(String p) throws Exception {
		int points = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM spvp_stats WHERE mojangid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			points = qr.rs.getInt("kills");
		}
		
		return points;
	}
	
	public int getDeaths(String p) throws Exception {
		int points = 0;
		
		QueryResult qr = HandlerStorage.getHandler("main").executeQuery("SELECT * FROM spvp_stats WHERE mojangid='" + PlayerUtilities.getUUID(p) + "'");
		qr.rs.last();
		if(qr.rs.getRow() != 0) {
			points = qr.rs.getInt("deaths");
		}
		
		return points;
	}
	
	public double getKD(String p) throws Exception {
		int kills = this.getKills(p);
		int deaths = this.getDeaths(p);
		
		if(deaths == 0){
			double kd = 0D;
			return kd;
		} else {
			double kd = kills / deaths;
			
			int decimalPlace = 2;
		    BigDecimal bd = new BigDecimal(kd);
		    bd = bd.setScale(decimalPlace,BigDecimal.ROUND_UP);
		    kd = bd.doubleValue();
			
			return kd;
		}
	}
	
	public void syncData(Player p) throws Exception {
		try {
			HandlerStorage.getHandler("main").execute("UPDATE `spvp_stats` SET `mojangid` = '" + PlayerUtilities.getUUID(p.getName()) + "' WHERE lastname='" + p.getName() + "'");
			//HandlerStorage.getHandler("main").execute("UPDATE `spvp_shopItems` SET `uuid` = '" + p.getUniqueId() + "' WHERE user='" + p.getName() + "'");
			HandlerStorage.getHandler("main").execute("UPDATE `spvp_stats` SET `lastname` = '" + p.getName() + "' WHERE mojangid='" + p.getUniqueId() + "'");
			//HandlerStorage.getHandler("main").execute("UPDATE `spvp_shopItems` SET `user` = '" + p.getName() + "' WHERE uuid='" + p.getUniqueId() + "'");
			System.out.println("[SPVP] Data sync succeded! Parameters: " + p.getName());
		} catch (Exception e){
			p.sendMessage(Vars.prefix + "�c" + "Something went wrong..");
		   	p.sendMessage(Vars.prefix + "�c" + "Take a look in the console!");
		  	e.printStackTrace();
		}
	}
	
	public void onDisable(){
		System.out.println("[SPVP 2.0] DISABLED!");
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String[] args){
		
		if((sender instanceof Player)){
			final Player p = (Player)sender;
			
			if(cmd.getName().equalsIgnoreCase("stats")){
				if(args.length == 0){
					if((sender instanceof Player)){
						Player pl = (Player)sender;
						String pn = pl.getName();
						
						try {
							sender.sendMessage(Vars.prefix + "�3========= �eStats von " + pl.getDisplayName() + " �3=========");
							sender.sendMessage(Vars.prefix + "�6Points: �e" + this.getPoints(pn));
							sender.sendMessage(Vars.prefix + "�6Kills: �e" + this.getKills(pn));
							sender.sendMessage(Vars.prefix + "�6Deaths: �e" + this.getDeaths(pn));
							sender.sendMessage(Vars.prefix + "�6K/D: �e" + this.getKD(pn));
							//sender.sendMessage(Vars.prefix + "�6H�chster Killstreak: �e" + this.getHighestStreak(p.getName()));
							sender.sendMessage(Vars.prefix + "�3========= �eStats von " + pl.getDisplayName() + " �3=========");
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						sender.sendMessage(PluginMeta.noplayer);
					}
				} else {
					if(PlayerUtilities.isOnline(args[0])){
						Player pl = Bukkit.getServer().getPlayer(args[0]);
						
						try {
							sender.sendMessage(Vars.prefix + "�3========= �eStats von " + pl.getDisplayName() + " �3=========");
							sender.sendMessage(Vars.prefix + "�6Points: �e" + this.getPoints(pl.getName()));
							sender.sendMessage(Vars.prefix + "�6Kills: �e" + this.getKills(pl.getName()));
							sender.sendMessage(Vars.prefix + "�6Deaths: �e" + this.getDeaths(pl.getName()));
							sender.sendMessage(Vars.prefix + "�6K/D: �e" + this.getKD(pl.getName()));
							//sender.sendMessage(Vars.prefix + "�6H�chster Killstreak: �e" + this.getHighestStreak(p.getName()));
							sender.sendMessage(Vars.prefix + "�3========= �eStats von " + pl.getDisplayName() + " �3=========");
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						OfflinePlayer pl = Bukkit.getServer().getOfflinePlayer(args[0]);
						try {
							String col = "�7";
							int r = PlayerUtilities.getRank(pl.getName());
							
							if(r == 0){
								col = "�7";
							} else if(r == 1){
								col = "�6";
							} else if(r == 2){
								col = "�6";
							} else if(r == 3){
								col = "�5";
							} else if(r == 4){
								col = "�5";
							} else if(r == 5){
								col = "�b";
							} else if(r == 6){
								col = "�3";
							} else if(r == 7){
								col = "�a";
							} else if(r == 8){
								col = "�e";
							} else if(r == 9){
								col = "�9";
							} else if(r == 10){
								col = "�c";
							} else if(r == 11){
								col = "�c";
							}
							sender.sendMessage(Vars.prefix + "�3========= �eStats von " + col + pl.getName() + " �3=========");
							sender.sendMessage(Vars.prefix + "�6Punkte: �e" + this.getPoints(pl.getName()));
							sender.sendMessage(Vars.prefix + "�6Kills: �e" + this.getKills(pl.getName()));
							sender.sendMessage(Vars.prefix + "�6Deaths: �e" + this.getDeaths(pl.getName()));
							sender.sendMessage(Vars.prefix + "�6K/D: �e" + this.getKD(pl.getName()));
							//sender.sendMessage(Vars.prefix + "�6H�chster Killstreak: �e" + this.getHighestStreak(p.getName()));
							sender.sendMessage(Vars.prefix + "�3========= �eStats von " + col + pl.getName() + " �3=========");
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
			
			if(cmd.getName().equalsIgnoreCase("setlobby")){
				if(p.hasPermission("spvp.setlobby")){
					Location lobby = p.getLocation();
					String world = lobby.getWorld().getName();
					double x = lobby.getX();
					double y = lobby.getY();
					double z = lobby.getZ();
					double yaw = lobby.getYaw();
					double pitch = lobby.getPitch();
					
					getConfig().set("SPVP.Locations.Lobby.world", world);
					getConfig().set("SPVP.Locations.Lobby.x", x);
					getConfig().set("SPVP.Locations.Lobby.y", y);
					getConfig().set("SPVP.Locations.Lobby.z", z);
					getConfig().set("SPVP.Locations.Lobby.yaw", yaw);
					getConfig().set("SPVP.Locations.Lobby.pitch", pitch);
					saveConfig();
					
					p.sendMessage(Vars.prefix + "==== Set Lobby to: ====");
					p.sendMessage(Vars.prefix + "�7World: �c" + world);
					p.sendMessage(Vars.prefix + "�7X: �c" + x);
					p.sendMessage(Vars.prefix + "�7Y: �c" + y);
					p.sendMessage(Vars.prefix + "�7Z: �c" + z);
					p.sendMessage(Vars.prefix + "�7Yaw: �c" + yaw);
					p.sendMessage(Vars.prefix + "�7Pitch: �c" + pitch);
				} else {
					p.sendMessage(Vars.nopermission);
				}
			}
			
			if(cmd.getName().equalsIgnoreCase("setspawn")){
				if(p.hasPermission("spvp.setspawn")){
					Location lobby = p.getLocation();
					String world = lobby.getWorld().getName();
					double x = lobby.getX();
					double y = lobby.getY();
					double z = lobby.getZ();
					double yaw = lobby.getYaw();
					double pitch = lobby.getPitch();
					
					getConfig().set("SPVP.Locations.Spawn.world", world);
					getConfig().set("SPVP.Locations.Spawn.x", x);
					getConfig().set("SPVP.Locations.Spawn.y", y);
					getConfig().set("SPVP.Locations.Spawn.z", z);
					getConfig().set("SPVP.Locations.Spawn.yaw", yaw);
					getConfig().set("SPVP.Locations.Spawn.pitch", pitch);
					saveConfig();
					
					p.sendMessage(Vars.prefix + "==== Set Spawn to: ====");
					p.sendMessage(Vars.prefix + "�7World: �c" + world);
					p.sendMessage(Vars.prefix + "�7X: �c" + x);
					p.sendMessage(Vars.prefix + "�7Y: �c" + y);
					p.sendMessage(Vars.prefix + "�7Z: �c" + z);
					p.sendMessage(Vars.prefix + "�7Yaw: �c" + yaw);
					p.sendMessage(Vars.prefix + "�7Pitch: �c" + pitch);
				} else {
					p.sendMessage(Vars.nopermission);
				}
			}
			
			if(cmd.getName().equalsIgnoreCase("clearingame")){
				if(p.hasPermission("spvp.clearingame")){
					this.is_ingame.clear();
					p.sendMessage(Vars.prefix + "�aSuccessfully cleared is_ingame() Array-List!");
				}
			}
			
			if(cmd.getName().equalsIgnoreCase("mysqlzeugs")){
				p.sendMessage(":O");
			}
			
			
		} else {
			sender.sendMessage(Vars.prefix + "Du musst ein 'Player' sein!");
		}
		
		return false;
	}
	
	@EventHandler
	public void onExplode1(EntityExplodeEvent e){
		if(e.getEntity() instanceof Fireball){
			e.setCancelled(true);
			((Fireball)e.getEntity()).setBounce(false);
			((Fireball)e.getEntity()).setYield(0F);
		}
	}
	
}
