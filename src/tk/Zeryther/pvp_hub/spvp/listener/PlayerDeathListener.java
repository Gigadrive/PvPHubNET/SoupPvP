package tk.Zeryther.pvp_hub.spvp.listener;

import net.pvp_hub.core.api.PlayerUtilities;

import org.bukkit.Bukkit;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import tk.Zeryther.pvp_hub.spvp.main.SoupPvP;
import tk.Zeryther.pvp_hub.spvp.main.Vars;

public class PlayerDeathListener implements Listener {
	
	private SoupPvP plugin;
	public PlayerDeathListener(SoupPvP plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		Player p = e.getEntity();
		if((p.getKiller() instanceof Player)){
			Player killer = p.getKiller();
			e.setDeathMessage(Vars.prefix + p.getDisplayName() + " �6wurde von " + p.getKiller().getDisplayName() + " �6get�tet!");
			try {
				plugin.removePoints(p.getName());
			} catch (Exception e1) {
				
			}
			
			try {
				if(PlayerUtilities.getRank(p.getName()) == 11 || PlayerUtilities.getRank(p.getName()) == 10){
					try {
						PlayerUtilities.achieve(4, p.getKiller());
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			} catch (Exception e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
			
			try {
				if(PlayerUtilities.getRank(p.getName()) == 3){
					PlayerUtilities.achieve(5, p.getKiller());
				}
			} catch (Exception e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
			
			try {
				if(PlayerUtilities.getRank(p.getName()) == 9){
					try {
						PlayerUtilities.achieve(3, p.getKiller());
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			} catch (Exception e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
			
			try {
				if(PlayerUtilities.getRank(p.getName()) == 4){
					PlayerUtilities.achieve(6, p.getKiller());
				}
			} catch (Exception e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}
			
			double khealth = ((Damageable)killer).getHealth();
			
			if(khealth == 20.0){
				try {
					PlayerUtilities.achieve(22, killer);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			try {
				plugin.addDeaths(p.getName());
				System.out.println(Vars.l_prefix + "Executed addDeaths() Method for Player " + p.getName());
			} catch (Exception e1) {
				System.err.println(Vars.l_prefix + "Failed to execute addDeaths() Method for Player " + p.getName());
				System.err.println(Vars.l_prefix + "Printing error log..");
				e1.printStackTrace();
				System.err.println(Vars.l_prefix + "Successfully printed error log!");
			}
			
			try {
				plugin.addKills(p.getKiller().getName());
				System.out.println(Vars.l_prefix + "Executed addKills() Method for Player " + p.getName());
			} catch (Exception e1) {
				System.err.println(Vars.l_prefix + "Failed to execute addPoints() Method for Player " + p.getName());
				System.err.println(Vars.l_prefix + "Printing error log..");
				e1.printStackTrace();
				System.err.println(Vars.l_prefix + "Successfully printed error log!");
			}
			
			try {
				plugin.addPoints(p.getKiller().getName());
				System.out.println(Vars.l_prefix + "Executed addPoints() Method for Player " + p.getName());
			} catch (Exception e1) {
				System.err.println(Vars.l_prefix + "Failed to execute addPoints() Method for Player " + p.getName());
				System.err.println(Vars.l_prefix + "Printing error log..");
				e1.printStackTrace();
				System.err.println(Vars.l_prefix + "Successfully printed error log!");
			}
			
			try {
				PlayerUtilities.addCoins(p.getKiller(), 3);
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
			int streakedKills = 0;
			if(SoupPvP.killStreak.containsKey(killer)){
				streakedKills = SoupPvP.killStreak.get(killer);
				SoupPvP.killStreak.remove(killer);
			}
			
			streakedKills++;
			SoupPvP.killStreak.put(killer, streakedKills);
			
			if(SoupPvP.killStreak.containsKey(p)){
				SoupPvP.killStreak.remove(p);
			}
			SoupPvP.killStreak.put(p, 0);
			
			if(streakedKills == 2){
				Bukkit.broadcastMessage(Vars.prefix + killer.getDisplayName() + " �6hat einen �aDOUBLE KILL�6! �a(2 Kills)");
				try {
					PlayerUtilities.addCoins(killer, 5);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else if(streakedKills == 3){
				Bukkit.broadcastMessage(Vars.prefix + killer.getDisplayName() + " �6hat einen TRIPLE KILL! �a(3 Kills)");
				try {
					PlayerUtilities.addCoins(killer, 10);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else if(streakedKills == 4){
				Bukkit.broadcastMessage(Vars.prefix + killer.getDisplayName() + " �6hat einen �eQUADRA KILL�6! �a(4 Kills)");
				try {
					PlayerUtilities.addCoins(killer, 20);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else if(streakedKills == 5){
				Bukkit.broadcastMessage(Vars.prefix + killer.getDisplayName() + " �6hat einen �cPENTA KILL�6! �a(5 Kills)");
				try {
					PlayerUtilities.addCoins(killer, 50);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else if(streakedKills == 7){
				Bukkit.broadcastMessage(Vars.prefix + killer.getDisplayName() + " �6hat einen �4UNBELIEVABLE KILL�6! �a(7 Kills)");
				try {
					PlayerUtilities.addCoins(killer, 70);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else if(streakedKills == 10){
				Bukkit.broadcastMessage(Vars.prefix + killer.getDisplayName() + " �6hat einen �4LEGENDARY KILL�6! �a(10 Kills)");
				try {
					PlayerUtilities.addCoins(killer, 100);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			p.sendMessage(Vars.prefix + "�cDu hast �e5 �cPunkte verloren!");
			p.getKiller().sendMessage(Vars.prefix + "�aDu hast �e5 �aPunkte f�r's T�ten von " + p.getDisplayName() + " �aerhalten!");
			try {
				p.sendMessage(Vars.prefix + "�cDu hast jetzt noch �e" + plugin.getPoints(p.getName()) + " �cPunkte!");
			} catch (Exception e1) {

			}
			
			try {
				p.getKiller().sendMessage(Vars.prefix + "�aDu hast jetzt �e" + plugin.getPoints(p.getKiller().getName()) + " �aPunkte!");
			} catch (Exception e1) {

			}
			
			killer.setLevel(streakedKills);
			killer.setExp((float) ((double) 60 / 60D));
			e.setDroppedExp(0);
			p.setLevel(0);
			p.setExp(0);
			
		} else {
			e.setDeathMessage(Vars.prefix + p.getDisplayName() + " �6ist gestorben!");
			try {
				plugin.removePoints(p.getName());
				p.sendMessage(Vars.prefix + "�cDu hast �e5 �cPunkte verloren!");
			} catch (Exception e1) {
				
			}
			
			try {
				plugin.addDeaths(p.getName());
				System.out.println(Vars.l_prefix + "Executed addDeaths() Method for Player " + p.getName());
			} catch (Exception e1) {
				System.err.println(Vars.l_prefix + "Failed to execute addDeaths() Method for Player " + p.getName());
				System.err.println(Vars.l_prefix + "Printing error log..");
				e1.printStackTrace();
				System.err.println(Vars.l_prefix + "Successfully printed error log!");
			}
			
			try {
				p.sendMessage(Vars.prefix + "�cDu hast jetzt noch �e" + plugin.getPoints(p.getName()) + " �cPunkte!");
			} catch (Exception e1) {

			}
			
			if(SoupPvP.killStreak.containsKey(p)){
				SoupPvP.killStreak.remove(p);
			}
			SoupPvP.killStreak.put(p, 0);
			
			p.setLevel(0);
			p.setExp(0);
			
			
		}
		
		plugin.removeFromArrayLists(p);
		
	}
	
}