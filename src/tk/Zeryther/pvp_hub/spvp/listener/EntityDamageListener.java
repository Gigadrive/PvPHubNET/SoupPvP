package tk.Zeryther.pvp_hub.spvp.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

import tk.Zeryther.pvp_hub.spvp.main.SoupPvP;

public class EntityDamageListener implements Listener {
	
	private SoupPvP plugin;
	public EntityDamageListener(SoupPvP plugin){
		this.plugin = plugin;
	}

	@EventHandler
	public void onDamage(EntityDamageEvent e){
		Player p = (Player) e.getEntity();
		
		if(plugin.is_ingame.contains(p.getName())){
			e.setCancelled(false);
		} else {
			e.setCancelled(true);
		} 
	}
	
}
