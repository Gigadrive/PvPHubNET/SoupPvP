package tk.Zeryther.pvp_hub.spvp.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

import tk.Zeryther.pvp_hub.spvp.main.SoupPvP;

public class WeatherChangeListener implements Listener {

	@SuppressWarnings("unused")
	private SoupPvP plugin;
	public WeatherChangeListener(SoupPvP plugin){
		this.plugin = plugin;
	}

	@EventHandler
	public void onWeatherChange(WeatherChangeEvent e){
		e.setCancelled(true);
		System.out.println("[LOGGER|SPVP] Cancelled WeatherChangeEvent!");
	}
}
