package tk.Zeryther.pvp_hub.spvp.listener;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import tk.Zeryther.pvp_hub.spvp.main.SoupPvP;

public class SecondInventoryClickListener implements Listener {

	private SoupPvP plugin;
	public SecondInventoryClickListener(SoupPvP plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e){
		
		Player p = (Player) e.getWhoClicked();
		
		if(e.getInventory().getName() == "suv"){
			
			if(e.getCurrentItem().getType() == Material.REDSTONE_BLOCK){
				e.setCancelled(true);
				p.closeInventory();
			} else if(e.getCurrentItem().getType() == Material.GOLD_BLOCK){
				p.sendMessage("b");
			} else if(e.getCurrentItem().getType() == Material.EMERALD_BLOCK){
				e.setCancelled(true);
			} else if(e.getCurrentItem().getType() == Material.IRON_BLOCK){
				p.sendMessage("a");
			} else if(e.getCurrentItem().equals(null)) {
				e.setCancelled(true);
				p.closeInventory();
			} else {
				e.setCancelled(true);
				p.closeInventory();
			}
			
		}
	}
	
}
