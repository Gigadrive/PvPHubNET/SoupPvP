package tk.Zeryther.pvp_hub.spvp.listener;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import tk.Zeryther.pvp_hub.spvp.main.SoupPvP;
import tk.Zeryther.pvp_hub.spvp.main.Vars;

public class InventoryClickListener implements Listener {
	
	public ArrayList<String> lore_coins = new ArrayList<String>();
	public ArrayList<String> lore_chips = new ArrayList<String>();
	
	public ArrayList<String> lore_info_survivor = new ArrayList<String>();
	
	public ArrayList<String> lore_chips_surv = new ArrayList<String>();
	public ArrayList<String> lore_coins_surv = new ArrayList<String>();
	
	private SoupPvP plugin;
	public InventoryClickListener(SoupPvP plugin){
		this.plugin = plugin;
	}
	
	public Inventory info_surv = null;

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onClick(InventoryClickEvent e){
		Player p = (Player)e.getWhoClicked();
		
		ItemStack mushroomSoup = new ItemStack(Material.MUSHROOM_SOUP, 1);
		ItemMeta mushMeta = mushroomSoup.getItemMeta();
		mushMeta.setDisplayName("�9�lHealing-Soup");
		mushroomSoup.setItemMeta(mushMeta);
		mushroomSoup.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
		
		try {
			if(e.getInventory().getName() == "�e�lSoupPvP �r�7- �3Kits"){
				e.setCancelled(true);
				if(e.getCurrentItem() == null){
					System.out.println("[LOGGER|SPVP] Successfully cancelled stack trace and NullPointerException in InventoryClickListener!");
				}
				
				// Kit - Assault
				if(e.getCurrentItem().getItemMeta().getDisplayName() == "�a�lAssault"){
					p.getInventory().clear();
					p.getInventory().setHelmet(new ItemStack(0, 1));
					p.getInventory().setChestplate(new ItemStack(0, 1));
					p.getInventory().setLeggings(new ItemStack(0, 1));
					p.getInventory().setBoots(new ItemStack(0, 1));
					
					ItemStack sword = new ItemStack(Material.IRON_SWORD, 1);
					ItemMeta swordMeta = sword.getItemMeta();
					swordMeta.setDisplayName("�a�lSchwert");
					sword.setItemMeta(swordMeta);
					sword.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
					sword.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
					p.getInventory().addItem(sword);
					
					for(int i = 1; i <= 71; i++) {
						p.getInventory().addItem(mushroomSoup);
					}
					
					p.getInventory().setHelmet(new ItemStack(Material.IRON_HELMET, 1));
					p.getInventory().setChestplate(new ItemStack(Material.IRON_CHESTPLATE, 1));
					p.getInventory().setLeggings(new ItemStack(Material.IRON_LEGGINGS, 1));
					p.getInventory().setBoots(new ItemStack(Material.IRON_BOOTS, 1));
					
					p.closeInventory();
					p.sendMessage(Vars.prefix + "�aDu hast das �lAssault" + ChatColor.RESET + "�a Kit erhalten.");
					
					String world = plugin.getConfig().getString("SPVP.Locations.Spawn.world");
					double x = plugin.getConfig().getDouble("SPVP.Locations.Spawn.x");
					double y = plugin.getConfig().getDouble("SPVP.Locations.Spawn.y");
					double z = plugin.getConfig().getDouble("SPVP.Locations.Spawn.z");
					double yaw = plugin.getConfig().getDouble("SPVP.Locations.Spawn.yaw");
					double pitch = plugin.getConfig().getDouble("SPVP.Locations.Spawn.pitch");
					
					Location spawn = new Location(Bukkit.getWorld(world), x, y, z);
					spawn.setYaw((float) yaw);
					spawn.setPitch((float) pitch);
					
					p.teleport(spawn);
					plugin.is_ingame.add(p.getName());
					plugin.class_assault.add(p);
				}
				
				if(e.getCurrentItem().getItemMeta().getDisplayName() == "�c�lArcher"){
					p.getInventory().clear();
					p.getInventory().setHelmet(new ItemStack(0, 1));
					p.getInventory().setChestplate(new ItemStack(0, 1));
					p.getInventory().setLeggings(new ItemStack(0, 1));
					p.getInventory().setBoots(new ItemStack(0, 1));
					
					ItemStack sword = new ItemStack(Material.WOOD_SWORD, 1);
					ItemMeta swordMeta = sword.getItemMeta();
					swordMeta.setDisplayName("�a�lSchwert");
					sword.setItemMeta(swordMeta);
					sword.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
					p.getInventory().addItem(sword);
					
					ItemStack bow = new ItemStack(Material.BOW, 1);
					ItemMeta bowMeta = bow.getItemMeta();
					bowMeta.setDisplayName("�c�lBogen");
					bow.setItemMeta(bowMeta);
					bow.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
					bow.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 2);
					p.getInventory().addItem(bow);
					
					for(int i = 1; i <= 71; i++) {
						p.getInventory().addItem(mushroomSoup);
					}
					
					p.getInventory().setItem(13, new ItemStack(Material.ARROW, 1));
					
					p.getInventory().setHelmet(new ItemStack(Material.CHAINMAIL_HELMET, 1));
					p.getInventory().setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1));
					p.getInventory().setLeggings(new ItemStack(Material.CHAINMAIL_LEGGINGS, 1));
					p.getInventory().setBoots(new ItemStack(Material.CHAINMAIL_BOOTS, 1));
					
					p.closeInventory();
					p.sendMessage(Vars.prefix + "�aDu hast das �c�lArcher" + ChatColor.RESET + "�a Kit erhalten.");
					
					String world = plugin.getConfig().getString("SPVP.Locations.Spawn.world");
					double x = plugin.getConfig().getDouble("SPVP.Locations.Spawn.x");
					double y = plugin.getConfig().getDouble("SPVP.Locations.Spawn.y");
					double z = plugin.getConfig().getDouble("SPVP.Locations.Spawn.z");
					double yaw = plugin.getConfig().getDouble("SPVP.Locations.Spawn.yaw");
					double pitch = plugin.getConfig().getDouble("SPVP.Locations.Spawn.pitch");
					
					Location spawn = new Location(Bukkit.getWorld(world), x, y, z);
					spawn.setYaw((float) yaw);
					spawn.setPitch((float) pitch);
					
					p.teleport(spawn);
					plugin.is_ingame.add(p.getName());
					plugin.class_archer.add(p);
				}
				
				if(e.getCurrentItem().getItemMeta().getDisplayName() == "�e�lChemist"){
					p.getInventory().clear();
					p.getInventory().setHelmet(new ItemStack(0, 1));
					p.getInventory().setChestplate(new ItemStack(0, 1));
					p.getInventory().setLeggings(new ItemStack(0, 1));
					p.getInventory().setBoots(new ItemStack(0, 1));
					
					ItemStack sword = new ItemStack(Material.WOOD_SWORD, 1);
					ItemMeta swordMeta = sword.getItemMeta();
					swordMeta.setDisplayName("�a�lSchwert");
					sword.setItemMeta(swordMeta);
					sword.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
					sword.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
					p.getInventory().addItem(sword);
					
					ItemStack p_dmg = new ItemStack(Material.POTION, 20, (short) 16460);
					ItemMeta p_dmgMeta = p_dmg.getItemMeta();
					p_dmgMeta.setDisplayName("�e�lWurftrank �7- �5Schaden");
					p_dmg.setItemMeta(p_dmgMeta);
					p.getInventory().addItem(p_dmg);
					
					ItemStack p_poi = new ItemStack(Material.POTION, 20, (short) 16388);
					ItemMeta p_poiMeta = p_poi.getItemMeta();
					p_poiMeta.setDisplayName("�e�lWurftrank �7- �aGift");
					p_poi.setItemMeta(p_poiMeta);
					p.getInventory().addItem(p_poi);
					
					ItemStack p_weak = new ItemStack(Material.POTION, 20, (short) 16424);
					ItemMeta p_weakMeta = p_weak.getItemMeta();
					p_weakMeta.setDisplayName("�e�lWurftrank �7- �8Schw�che");
					p_weak.setItemMeta(p_weakMeta);
					p.getInventory().addItem(p_weak);
					
					for(int i = 1; i <= 71; i++) {
						p.getInventory().addItem(mushroomSoup);
					}
					
					p.getInventory().setHelmet(new ItemStack(306, 1));
					p.getInventory().setChestplate(new ItemStack(312, 1));
					p.getInventory().setLeggings(new ItemStack(309, 1));
					p.getInventory().setBoots(new ItemStack(301, 1));
					
					p.closeInventory();
					p.sendMessage(Vars.prefix + "�aDu hast das �c�lArcher" + ChatColor.RESET + "�a Kit erhalten.");
					
					String world = plugin.getConfig().getString("SPVP.Locations.Spawn.world");
					double x = plugin.getConfig().getDouble("SPVP.Locations.Spawn.x");
					double y = plugin.getConfig().getDouble("SPVP.Locations.Spawn.y");
					double z = plugin.getConfig().getDouble("SPVP.Locations.Spawn.z");
					double yaw = plugin.getConfig().getDouble("SPVP.Locations.Spawn.yaw");
					double pitch = plugin.getConfig().getDouble("SPVP.Locations.Spawn.pitch");
					
					Location spawn = new Location(Bukkit.getWorld(world), x, y, z);
					spawn.setYaw((float) yaw);
					spawn.setPitch((float) pitch);
					
					p.teleport(spawn);
					plugin.is_ingame.add(p.getName());
					plugin.class_chemist.add(p);
				}
				
				if(e.getCurrentItem().getItemMeta().getDisplayName() == "�2�lAssist"){
					p.getInventory().clear();
					p.getInventory().setHelmet(new ItemStack(0, 1));
					p.getInventory().setChestplate(new ItemStack(0, 1));
					p.getInventory().setLeggings(new ItemStack(0, 1));
					p.getInventory().setBoots(new ItemStack(0, 1));
					
					ItemStack sword = new ItemStack(Material.STONE_SWORD, 1);
					ItemMeta swordMeta = sword.getItemMeta();
					swordMeta.setDisplayName("�a�lSchwert");
					sword.setItemMeta(swordMeta);
					sword.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
					p.getInventory().addItem(sword);
					
					ItemStack apple = new ItemStack(Material.APPLE, 1);
					ItemMeta appleMeta = apple.getItemMeta();
					appleMeta.setDisplayName("�c�lApfel �eder �2Heilung");
					apple.setItemMeta(appleMeta);
					p.getInventory().addItem(apple);
					
					for(int i = 1; i <= 71; i++) {
						p.getInventory().addItem(mushroomSoup);
					}
					
					ItemStack wiz_helm = new ItemStack(Material.LEATHER_HELMET, 1);
					LeatherArmorMeta wiz_helmMeta = (LeatherArmorMeta)wiz_helm.getItemMeta();
					wiz_helmMeta.setColor(Color.fromRGB(0, 190, 0));
					wiz_helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
					wiz_helm.setItemMeta(wiz_helmMeta);
					
					ItemStack wiz_chest = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
					wiz_chest.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
					wiz_chest.setItemMeta(wiz_helmMeta);
					
					ItemStack wiz_leg = new ItemStack(Material.LEATHER_LEGGINGS, 1);
					wiz_leg.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
					wiz_leg.setItemMeta(wiz_helmMeta);
					
					ItemStack wiz_boot = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
					wiz_boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
					wiz_boot.setItemMeta(wiz_helmMeta);
					
					p.getInventory().setHelmet(new ItemStack(wiz_helm));
					p.getInventory().setChestplate(new ItemStack(wiz_chest));
					p.getInventory().setLeggings(new ItemStack(wiz_leg));
					p.getInventory().setBoots(new ItemStack(wiz_boot));
					
					p.closeInventory();
					p.sendMessage(Vars.prefix + "�aDu hast das �2�lAssist" + ChatColor.RESET + "�a Kit erhalten.");
					
					String world = plugin.getConfig().getString("SPVP.Locations.Spawn.world");
					double x = plugin.getConfig().getDouble("SPVP.Locations.Spawn.x");
					double y = plugin.getConfig().getDouble("SPVP.Locations.Spawn.y");
					double z = plugin.getConfig().getDouble("SPVP.Locations.Spawn.z");
					double yaw = plugin.getConfig().getDouble("SPVP.Locations.Spawn.yaw");
					double pitch = plugin.getConfig().getDouble("SPVP.Locations.Spawn.pitch");
					
					Location spawn = new Location(Bukkit.getWorld(world), x, y, z);
					spawn.setYaw((float) yaw);
					spawn.setPitch((float) pitch);
					
					p.teleport(spawn);
					plugin.is_ingame.add(p.getName());
					plugin.class_assist.add(p);
				}
				
				if(e.getCurrentItem().getItemMeta().getDisplayName() == "�d�lAllround"){
					p.getInventory().clear();
					p.getInventory().setHelmet(new ItemStack(0, 1));
					p.getInventory().setChestplate(new ItemStack(0, 1));
					p.getInventory().setLeggings(new ItemStack(0, 1));
					p.getInventory().setBoots(new ItemStack(0, 1));
					
					ItemStack sword = new ItemStack(Material.IRON_SWORD, 1);
					ItemMeta swordMeta = sword.getItemMeta();
					swordMeta.setDisplayName("�a�lSchwert");
					sword.setItemMeta(swordMeta);
					sword.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
					p.getInventory().addItem(sword);
					
					ItemStack bow = new ItemStack(Material.BOW, 1);
					ItemMeta bowMeta = bow.getItemMeta();
					bowMeta.setDisplayName("�c�lBogen");
					bow.setItemMeta(bowMeta);
					bow.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
					p.getInventory().addItem(bow);
					
					for(int i = 1; i <= 71; i++) {
						p.getInventory().addItem(mushroomSoup);
					}
					p.getInventory().setItem(13, new ItemStack(Material.ARROW, 1));
					
					p.getInventory().setHelmet(new ItemStack(Material.CHAINMAIL_HELMET, 1));
					p.getInventory().setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1));
					p.getInventory().setLeggings(new ItemStack(Material.IRON_LEGGINGS, 1));
					p.getInventory().setBoots(new ItemStack(Material.IRON_BOOTS, 1));
					
					p.closeInventory();
					p.sendMessage(Vars.prefix + "�aDu hast das �d�lAllround" + ChatColor.RESET + "�a Kit erhalten.");
					
					String world = plugin.getConfig().getString("SPVP.Locations.Spawn.world");
					double x = plugin.getConfig().getDouble("SPVP.Locations.Spawn.x");
					double y = plugin.getConfig().getDouble("SPVP.Locations.Spawn.y");
					double z = plugin.getConfig().getDouble("SPVP.Locations.Spawn.z");
					double yaw = plugin.getConfig().getDouble("SPVP.Locations.Spawn.yaw");
					double pitch = plugin.getConfig().getDouble("SPVP.Locations.Spawn.pitch");
					
					Location spawn = new Location(Bukkit.getWorld(world), x, y, z);
					spawn.setYaw((float) yaw);
					spawn.setPitch((float) pitch);
					
					p.teleport(spawn);
					plugin.is_ingame.add(p.getName());
					plugin.class_allround.add(p);
				}
				
				if(e.getCurrentItem().getItemMeta().getDisplayName() == "�3�lSurvivor"){
							p.getInventory().clear();
							p.getInventory().setHelmet(new ItemStack(0, 1));
							p.getInventory().setChestplate(new ItemStack(0, 1));
							p.getInventory().setLeggings(new ItemStack(0, 1));
							p.getInventory().setBoots(new ItemStack(Material.IRON_BOOTS, 1));
							
							ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1);
							ItemMeta swordMeta = sword.getItemMeta();
							swordMeta.setDisplayName("�a�lSchwert");
							sword.setItemMeta(swordMeta);
							sword.addUnsafeEnchantment(Enchantment.KNOCKBACK, 2);
							sword.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 4);
							sword.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
							p.getInventory().addItem(sword);
							p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1));
							p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE, 2));
							
							for(int i = 1; i <= 71; i++) {
								p.getInventory().addItem(mushroomSoup);
							}
							
							p.closeInventory();
							p.sendMessage(Vars.prefix + "�aDu hast das �3�lSurvivor" + ChatColor.RESET + "�a Kit erhalten.");
							
							String world = plugin.getConfig().getString("SPVP.Locations.Spawn.world");
							double x = plugin.getConfig().getDouble("SPVP.Locations.Spawn.x");
							double y = plugin.getConfig().getDouble("SPVP.Locations.Spawn.y");
							double z = plugin.getConfig().getDouble("SPVP.Locations.Spawn.z");
							double yaw = plugin.getConfig().getDouble("SPVP.Locations.Spawn.yaw");
							double pitch = plugin.getConfig().getDouble("SPVP.Locations.Spawn.pitch");
							
							Location spawn = new Location(Bukkit.getWorld(world), x, y, z);
							spawn.setYaw((float) yaw);
							spawn.setPitch((float) pitch);
							
							p.teleport(spawn);
							plugin.is_ingame.add(p.getName());
							plugin.class_survivor.add(p);
				}
				
				if(e.getCurrentItem().getItemMeta().getDisplayName() == "�5�lWizard"){
							p.getInventory().clear();
							p.getInventory().setHelmet(new ItemStack(0, 1));
							p.getInventory().setChestplate(new ItemStack(0, 1));
							p.getInventory().setLeggings(new ItemStack(0, 1));
							p.getInventory().setBoots(new ItemStack(0, 1));
							
							ItemStack stick = new ItemStack(Material.STICK, 1);
							ItemMeta stickMeta = stick.getItemMeta();
							stickMeta.setDisplayName("�5�lZauberstab");
							stick.addUnsafeEnchantment(Enchantment.KNOCKBACK, 2);
							stick.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
							stick.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 3);
							stick.setItemMeta(stickMeta);
							p.getInventory().addItem(stick);
							
							ItemStack p_spd = new ItemStack(Material.POTION, 1, (short) 8226);
							ItemMeta p_spdMeta = p_spd.getItemMeta();
							p_spdMeta.setDisplayName("�e�lTrank �7- �bGeschwindigkeit");
							p_spd.setItemMeta(p_spdMeta);
							p.getInventory().addItem(p_spd);
							
							for(int i = 1; i <= 71; i++) {
								p.getInventory().addItem(mushroomSoup);
							}
							
							p.getInventory().setHelmet(new ItemStack(Material.CHAINMAIL_HELMET, 1));
							p.getInventory().setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1));
							p.getInventory().setLeggings(new ItemStack(Material.IRON_LEGGINGS, 1));
							p.getInventory().setBoots(new ItemStack(Material.IRON_BOOTS, 1));
							
							p.closeInventory();
							p.sendMessage(Vars.prefix + "�aDu hast das �5�lWizard" + ChatColor.RESET + "�a Kit erhalten.");
							
							String world = plugin.getConfig().getString("SPVP.Locations.Spawn.world");
							double x = plugin.getConfig().getDouble("SPVP.Locations.Spawn.x");
							double y = plugin.getConfig().getDouble("SPVP.Locations.Spawn.y");
							double z = plugin.getConfig().getDouble("SPVP.Locations.Spawn.z");
							double yaw = plugin.getConfig().getDouble("SPVP.Locations.Spawn.yaw");
							double pitch = plugin.getConfig().getDouble("SPVP.Locations.Spawn.pitch");
							
							Location spawn = new Location(Bukkit.getWorld(world), x, y, z);
							spawn.setYaw((float) yaw);
							spawn.setPitch((float) pitch);
							
							p.teleport(spawn);
							plugin.is_ingame.add(p.getName());
							plugin.class_wizard.add(p);
						}
				
				
				if(e.getCurrentItem().getItemMeta().getDisplayName() == "�b�lHeavy"){
						p.getInventory().clear();
						p.getInventory().setHelmet(new ItemStack(0, 1));
						p.getInventory().setChestplate(new ItemStack(0, 1));
						p.getInventory().setLeggings(new ItemStack(0, 1));
						p.getInventory().setBoots(new ItemStack(0, 1));
						
						ItemStack sword_heavy = new ItemStack(Material.WOOD_SWORD, 1);
						ItemMeta sword_heavyMeta = sword_heavy.getItemMeta();
						sword_heavyMeta.setDisplayName("�a�lSchwert");
						sword_heavy.setItemMeta(sword_heavyMeta);
						sword_heavy.addUnsafeEnchantment(Enchantment.DURABILITY, 5);
						p.getInventory().addItem(sword_heavy);
						
						for(int i = 1; i <= 71; i++) {
							p.getInventory().addItem(mushroomSoup);
						}
						
						ItemStack h = new ItemStack(Material.DIAMOND_HELMET);
						h.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
						
						ItemStack c = new ItemStack(Material.DIAMOND_CHESTPLATE);
						c.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
						
						ItemStack l = new ItemStack(Material.DIAMOND_LEGGINGS);
						l.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
						
						ItemStack b = new ItemStack(Material.DIAMOND_BOOTS);
						b.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
						
						p.getInventory().setHelmet(h);
						p.getInventory().setChestplate(c);
						p.getInventory().setLeggings(l);
						p.getInventory().setBoots(b);
						
						p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 1));
						
						p.closeInventory();
						p.sendMessage(Vars.prefix + "�aDu hast das �b�lHeavy" + ChatColor.RESET + "�a Kit erhalten.");
						
						String world = plugin.getConfig().getString("SPVP.Locations.Spawn.world");
						double x = plugin.getConfig().getDouble("SPVP.Locations.Spawn.x");
						double y = plugin.getConfig().getDouble("SPVP.Locations.Spawn.y");
						double z = plugin.getConfig().getDouble("SPVP.Locations.Spawn.z");
						double yaw = plugin.getConfig().getDouble("SPVP.Locations.Spawn.yaw");
						double pitch = plugin.getConfig().getDouble("SPVP.Locations.Spawn.pitch");
						
						Location spawn = new Location(Bukkit.getWorld(world), x, y, z);
						spawn.setYaw((float) yaw);
						spawn.setPitch((float) pitch);
						
						p.teleport(spawn);
						plugin.is_ingame.add(p.getName());
						plugin.class_heavy.add(p);
				}
				
				if(e.getCurrentItem().getItemMeta().getDisplayName().equals(" ")){
					e.setCancelled(true);
				}
			}
			
			if(e.getInventory().getName() == "�e�lSoupPvP �r�7- �3REFILL"){
				e.setCancelled(false);
			}
		} catch (Exception e1){ }
	}
	
}
