package tk.Zeryther.pvp_hub.spvp.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import tk.Zeryther.pvp_hub.spvp.main.SoupPvP;

public class AsyncPlayerChatListener implements Listener {

	private SoupPvP plugin;
	public AsyncPlayerChatListener(SoupPvP plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e){
		Player p = e.getPlayer();
		
		e.setCancelled(true);
		Bukkit.broadcastMessage("�8<[" + plugin.getClassChatFormat(p) + "�8] �r" + p.getDisplayName() + "�r�8> �f" + e.getMessage());
	}
	
}
