package tk.Zeryther.pvp_hub.spvp.listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import tk.Zeryther.pvp_hub.spvp.main.SoupPvP;
import tk.Zeryther.pvp_hub.spvp.main.Vars;

public class PlayerInteractEntityListener implements Listener {
	
	private SoupPvP plugin;
	public PlayerInteractEntityListener(SoupPvP plugin){
		this.plugin = plugin;
	}

	@EventHandler
	public void onUseHealApple(PlayerInteractEntityEvent e){
		final Player p = e.getPlayer();
		
		if(e.getRightClicked() instanceof Player){
			Player p2 = (Player) e.getRightClicked();
			if(p.getItemInHand().getItemMeta().getDisplayName() == "�c�lApfel �eder �2Heilung"){
				p2.setHealth(20.0);
				p2.sendMessage(Vars.prefix + "�aDu wurdest von " + p.getName() + " geheilt");
				p.sendMessage(Vars.prefix + "�aDu hast " + p2.getName() + " geheilt");
				p.sendMessage(Vars.prefix + "�aDu kannst in 10 Sekunden wieder jemanden heilen.");
				p.getInventory().removeItem(p.getItemInHand());
				
				plugin.PID = Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
					@Override
					public void run() {
						ItemStack apple_assist = new ItemStack(Material.APPLE, 1);
						ItemMeta apple_assistMeta = apple_assist.getItemMeta();
						apple_assistMeta.setDisplayName("�c�lApfel �eder �2Heilung");
						apple_assist.setItemMeta(apple_assistMeta);
						p.getInventory().addItem(apple_assist);
					}
				}, 200L);
			}
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onWizardStick(PlayerInteractEvent e){
		final Player p = e.getPlayer();
		
		if(plugin.is_ingame.contains(p.getName())){
			if(e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK){
				if(p.getItemInHand().getItemMeta().getDisplayName() == "�5�lZauberstab"){
					if(!plugin.used_arrow.contains(p.getName())){
						p.shootArrow();
						plugin.used_arrow.add(p.getName());
						
						p.sendMessage(Vars.prefix + "Du hast den �eARROW-SKILL �6benutzt!");
						p.sendMessage(Vars.prefix + "Du kannst diesen in 1.5 Sekunden wieder benutzen.");
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							
							@Override
							public void run(){
								plugin.used_arrow.remove(p.getName());
								p.sendMessage(Vars.prefix + "Du kannst nun wieder den �eARROW-SKILL �6benutzen!");
							}
							
						}, 30);
					}
					
					if(plugin.used_fireball.contains(p.getName())){
						p.sendMessage("�cDer �4FIREBALL-SKILL �cmuss nachladen..");
					}
				}
			} else if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK){
				if(p.getItemInHand().getItemMeta().getDisplayName() == "�5�lZauberstab"){
					if(!plugin.used_fireball.contains(p.getName())){
						p.launchProjectile(Fireball.class);
						plugin.used_fireball.add(p.getName());
						
						p.sendMessage(Vars.prefix + "Du hast den �4FIREBALL-SKILL �6benutzt!");
						p.sendMessage(Vars.prefix + "Du kannst diesen in 10 Sekunden wieder benutzen.");
						Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							
							@Override
							public void run(){
								plugin.used_fireball.remove(p.getName());
								p.sendMessage(Vars.prefix + "Du kannst nun wieder den �4FIREBALL-SKILL �6benutzen!");
							}
							
						}, 20*10);
					}
					
					if(plugin.used_fireball.contains(p.getName())){
						p.sendMessage("�cDer �4FIREBALL-SKILL �cmuss nachladen..");
					}
				}
			}
		}
	}
	
}
