package tk.Zeryther.pvp_hub.spvp.listener;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import tk.Zeryther.pvp_hub.spvp.main.SoupPvP;

public class PlayerDropListener implements Listener {
	
	private SoupPvP plugin;
	public PlayerDropListener(SoupPvP plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent e){
		if(e.getItemDrop().getItemStack().getType() == Material.BOWL){
			e.setCancelled(false);
		} else {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPickUp(PlayerPickupItemEvent e){
		e.setCancelled(true);
	}
	
}
