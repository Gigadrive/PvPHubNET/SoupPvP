package tk.Zeryther.pvp_hub.spvp.listener;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;

import tk.Zeryther.pvp_hub.spvp.main.SoupPvP;
import tk.Zeryther.pvp_hub.spvp.main.Vars;

public class AsyncPlayerPreLoginListener implements Listener {

	public String uuid;
	
	private SoupPvP plugin;
	public AsyncPlayerPreLoginListener(SoupPvP plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPreLogin(AsyncPlayerPreLoginEvent e){
		this.uuid = e.getUniqueId().toString().replace("-", "");
		System.out.println(Vars.l_prefix + e.getName() + "s UUID: " + this.uuid);
	}
	
}
