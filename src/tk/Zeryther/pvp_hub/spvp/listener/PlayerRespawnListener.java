package tk.Zeryther.pvp_hub.spvp.listener;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import tk.Zeryther.pvp_hub.spvp.main.SoupPvP;

public class PlayerRespawnListener implements Listener {

	private SoupPvP plugin;
	public PlayerRespawnListener(SoupPvP plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e){
		Player p = e.getPlayer();
		
		p.getInventory().clear();
		p.getInventory().setHelmet(new ItemStack(Material.AIR));
		p.getInventory().setChestplate(new ItemStack(Material.AIR));
		p.getInventory().setLeggings(new ItemStack(Material.AIR));
		p.getInventory().setBoots(new ItemStack(Material.AIR));
		
		String world = "world";
		double x = 73.9330D;
		double y = 27.0D;
		double z = -492.4333D;
		double yaw = -1.8431D;
		double pitch = 3.6864D;
		
		Location lobby = new Location(Bukkit.getWorld(world), x, y, z);
		lobby.setYaw((float) yaw);
		lobby.setPitch((float) pitch);
		
		p.teleport(lobby);
		
		if(!(p.getGameMode() == GameMode.ADVENTURE)){
			p.setGameMode(GameMode.ADVENTURE);
		}
	}
	
}
