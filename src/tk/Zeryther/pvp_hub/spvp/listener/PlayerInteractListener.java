package tk.Zeryther.pvp_hub.spvp.listener;

import java.io.IOException;
import java.util.ArrayList;

import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.core.api.PluginMeta;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;

import tk.Zeryther.pvp_hub.spvp.main.SoupPvP;
import tk.Zeryther.pvp_hub.spvp.main.Vars;
import tk.Zeryther.pvp_hub.spvp.menus.KitList;

public class PlayerInteractListener implements Listener {
	
	public ArrayList<String> lore_coins = new ArrayList<String>();
	public ArrayList<String> lore_chips = new ArrayList<String>();
	
	public ArrayList<String> lore_survivor = new ArrayList<String>();
	public ArrayList<String> lore_wizard = new ArrayList<String>();
	
	private Inventory shop = null;
	
	private Inventory rf;
	
	private SoupPvP plugin;
	public PlayerInteractListener(SoupPvP plugin){
		this.plugin = plugin;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onSign(PlayerInteractEvent e){
		final Player p = e.getPlayer();
		
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
	        if (e.getClickedBlock().getType() == Material.SIGN || e.getClickedBlock().getType() == Material.SIGN_POST || e.getClickedBlock().getType() == Material.WALL_SIGN) {
	        	Sign sign = (Sign) e.getClickedBlock().getState();
	        	
	        	if(sign.getLine(0).equalsIgnoreCase("[spvp]") && sign.getLine(1).equalsIgnoreCase("leave")){
	        		sign.setLine(0, "");
	        		sign.setLine(1, "�e[SoupPvP]");
	        		sign.setLine(2, "Arena verlassen");
	        		sign.setLine(3, "");
	        		sign.update();
	        		p.sendMessage(Vars.prefix + "Sign created!");
	        	}
	        	
	        	if(sign.getLine(0).equalsIgnoreCase("[spvp]") && sign.getLine(1).equalsIgnoreCase("stats")){
	        		sign.setLine(0, "");
	        		sign.setLine(1, "�e[SoupPvP]");
	        		sign.setLine(2, "Statistiken");
	        		sign.setLine(3, "");
	        		sign.update();
	        		p.sendMessage(Vars.prefix + "Sign created!");
	        	}
	        	
	        	if(sign.getLine(0).equalsIgnoreCase("[spvp]") && sign.getLine(1).equalsIgnoreCase("toplist")){
	        		sign.setLine(0, "");
	        		sign.setLine(1, "�e[SoupPvP]");
	        		sign.setLine(2, "Topliste");
	        		sign.setLine(3, "");
	        		sign.update();
	        		p.sendMessage(Vars.prefix + "Sign created!");
	        	}
	        	
	        	if(sign.getLine(0).equalsIgnoreCase("[spvp]") && sign.getLine(1).equalsIgnoreCase("join_left")){
	        		sign.setLine(0, "");
	        		sign.setLine(1, "�lIn die Arena");
	        		sign.setLine(2, "�9�l---->");
	        		sign.setLine(3, "");
	        		sign.update();
	        		p.sendMessage(Vars.prefix + "Sign created!");
	        	}
	        	
	        	if(sign.getLine(0).equalsIgnoreCase("[spvp]") && sign.getLine(1).equalsIgnoreCase("join")){
	        		sign.setLine(0, "");
	        		sign.setLine(1, "�e[SoupPvP]");
	        		sign.setLine(2, "Arena betreten");
	        		sign.setLine(3, "");
	        		sign.update();
	        		p.sendMessage(Vars.prefix + "Sign created!");
	        	}
	        	
	        	if(sign.getLine(0).equalsIgnoreCase("[spvp]") && sign.getLine(1).equalsIgnoreCase("lobby")){
	        		sign.setLine(0, "");
	        		sign.setLine(1, "�4[PvP-Hub]");
	        		sign.setLine(2, "Zur Lobby");
	        		sign.setLine(3, "");
	        		sign.update();
	        		p.sendMessage(Vars.prefix + "Sign created!");
	        	}
	        	
	        	if(sign.getLine(0).equalsIgnoreCase("[spvp]") && sign.getLine(1).equalsIgnoreCase("shop")){
	        		sign.setLine(0, "");
	        		sign.setLine(1, "�e[SoupPvP]");
	        		sign.setLine(2, "Shop");
	        		sign.setLine(3, "");
	        		sign.update();
	        		p.sendMessage(Vars.prefix + "Sign created!");
	        	}
	        	
	        	if(sign.getLine(0).equalsIgnoreCase("[spvp]") && sign.getLine(1).equalsIgnoreCase("refill")){
	        		sign.setLine(0, "");
	        		sign.setLine(1, "�e[SoupPvP]");
	        		sign.setLine(2, "Suppen REFILL!");
	        		sign.setLine(3, "");
	        		sign.update();
	        		p.sendMessage(Vars.prefix + "Sign created!");
	        	}
	        	
	        	if (sign.getLine(1).equalsIgnoreCase("�e[SoupPvP]") && sign.getLine(2).equalsIgnoreCase("Suppen REFILL!")) {
	        		ItemStack mushroomSoup = new ItemStack(Material.MUSHROOM_SOUP, 1);
	        		ItemMeta mushMeta = mushroomSoup.getItemMeta();
	        		mushMeta.setDisplayName("�9�lHealing-Soup");
	        		mushroomSoup.setItemMeta(mushMeta);
	        		mushroomSoup.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 1);
	        		
	        		rf = Bukkit.createInventory(null, 45, "�e�lSoupPvP �r�7- �3REFILL");
	    			for(int i = 1; i <= 46; i++) {
	    				rf.addItem(mushroomSoup);
	    			}
	    			
	    			p.openInventory(rf);
	            }
	        	
	        	if (sign.getLine(1).equalsIgnoreCase("�e[SoupPvP]") && sign.getLine(2).equalsIgnoreCase("Statistiken")) {
	                p.performCommand("stats");
	            }
	        	
	        	if (sign.getLine(1).equalsIgnoreCase("�e[SoupPvP]") && sign.getLine(2).equalsIgnoreCase("Topliste")) {
	        		p.sendMessage(Vars.prefix + "Diese Funktion wurde auf");
	        		p.sendMessage(Vars.prefix + "�7- �6https://pvp-hub.net/gametypes/souppvp/toplist");
	        		p.sendMessage(Vars.prefix + "verlegt!");
	            }
	        	
	        	if (sign.getLine(1).equalsIgnoreCase("�e[SoupPvP]") && sign.getLine(2).equalsIgnoreCase("Arena betreten")) {
	        		plugin.removeFromArrayLists(p);
	            	KitList.openFor(p);
	            }
	        	
	        	if (sign.getLine(1).equalsIgnoreCase("�e[SoupPvP]") && sign.getLine(2).equalsIgnoreCase("Arena verlassen")) {
	        		String world = plugin.getConfig().getString("SPVP.Locations.Lobby.world");
	        		double x = plugin.getConfig().getDouble("SPVP.Locations.Lobby.x");
	        		double y = plugin.getConfig().getDouble("SPVP.Locations.Lobby.y");
	        		double z = plugin.getConfig().getDouble("SPVP.Locations.Lobby.z");
	        		double yaw = plugin.getConfig().getDouble("SPVP.Locations.Lobby.yaw");
	        		double pitch = plugin.getConfig().getDouble("SPVP.Locations.Lobby.pitch");
	        		
	        		Location login = new Location(Bukkit.getWorld(world), x, y, z);
	        		login.setYaw((float) yaw);
	        		login.setPitch((float) pitch);
	        		
	        		p.teleport(login);
	        		
	        		p.setHealth(20.0);
	        		p.setFoodLevel(20);
	        		p.setFireTicks(0);
	        		
	        		for (PotionEffect effect : e.getPlayer().getActivePotionEffects()) {
	        			e.getPlayer().removePotionEffect(effect.getType());
	        		}

	        		plugin.removeFromArrayLists(p);
					p.getInventory().clear();
					p.getInventory().setItem(0, new ItemStack(0, 1));
					p.getInventory().setHelmet(new ItemStack(0, 1));
					p.getInventory().setChestplate(new ItemStack(0, 1));
					p.getInventory().setLeggings(new ItemStack(0, 1));
					p.getInventory().setBoots(new ItemStack(0, 1));
					Bukkit.getScheduler().scheduleAsyncDelayedTask(plugin, new Runnable(){
						@Override
						public void run(){
							int i = p.getInventory().getHeldItemSlot();
							p.getInventory().setItem(i, new ItemStack(Material.AIR, 1));
						}
					}, 2L);
	            }
	        	
	        	if (sign.getLine(1).equalsIgnoreCase("�4[PvP-Hub]") && sign.getLine(2).equalsIgnoreCase("Zur Lobby")) {
	        		try {
						PlayerUtilities.connect(plugin, "lobby", p);
						p.sendMessage(PluginMeta.prefix + "�aTeleportiere dich auf einen Lobby Server..");
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
	            }
	        }
	        }
	}
	
	@EventHandler
	public void onSoup(PlayerInteractEvent event){
	    if ((event.getAction() == Action.RIGHT_CLICK_AIR) || (event.getAction() == Action.RIGHT_CLICK_BLOCK)){
	      int heal = 7;
	      int feed = 7;
	      if (event.getPlayer().getItemInHand().getType() == Material.MUSHROOM_SOUP){
	        ItemStack bowl = new ItemStack(Material.BOWL, 1);
	        ItemMeta meta = bowl.getItemMeta();
	        if ((((Damageable) event.getPlayer()).getHealth() < 20) && (((Damageable) event.getPlayer()).getHealth() > 0))
	        {
	          if (((Damageable) event.getPlayer()).getHealth() < 20 - heal + 1)
	          {
	            event.getPlayer().getItemInHand().setType(Material.BOWL);
	            event.getPlayer().getItemInHand().setItemMeta(meta);
	            event.getPlayer().setItemInHand(bowl);
	            event.getPlayer().setHealth(((Damageable) event.getPlayer()).getHealth() + heal);
	            event.getPlayer().getItemInHand().setType(Material.AIR);
	          }
	          else if ((((Damageable) event.getPlayer()).getHealth() < 20.0) && (((Damageable) event.getPlayer()).getHealth() > 20 - heal))
	          {
	            event.getPlayer().setHealth(20.0);
	            event.getPlayer().getItemInHand().setType(Material.BOWL);
	            event.getPlayer().getItemInHand().setItemMeta(meta);
	            event.getPlayer().getItemInHand().setType(Material.AIR);
	          }
	        }
	        else if ((((Damageable) event.getPlayer()).getHealth() == 20) && (event.getPlayer().getFoodLevel() < 20)) {
	          if (event.getPlayer().getFoodLevel() < 20 - feed + 1)
	          {
	            event.getPlayer().setFoodLevel(event.getPlayer().getFoodLevel() + feed);
	            event.getPlayer().getItemInHand().setType(Material.BOWL);
	            event.getPlayer().getItemInHand().setItemMeta(meta);
	            event.getPlayer().getItemInHand().setType(Material.AIR);
	          }
	          else if ((event.getPlayer().getFoodLevel() < 20) && (event.getPlayer().getFoodLevel() > 20 - feed))
	          {
	            event.getPlayer().setFoodLevel(20);
	            event.getPlayer().getItemInHand().setType(Material.BOWL);
	            event.getPlayer().getItemInHand().setItemMeta(meta);
	            event.getPlayer().getItemInHand().setType(Material.AIR);
	          }
	        }
	      }
	    }
	}
	
}
