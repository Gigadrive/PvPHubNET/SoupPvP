package tk.Zeryther.pvp_hub.spvp.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import tk.Zeryther.pvp_hub.spvp.main.SoupPvP;
import tk.Zeryther.pvp_hub.spvp.main.Vars;

public class PlayerDisconnectListener implements Listener {

	private SoupPvP plugin;
	public PlayerDisconnectListener(SoupPvP plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onDisconnect(PlayerQuitEvent e){
		Player p = e.getPlayer();
		if(plugin.is_ingame.contains(e.getPlayer().getName())){
			plugin.is_ingame.remove(e.getPlayer().getName());
		}
		
		e.setQuitMessage(Vars.prefix + e.getPlayer().getDisplayName() + " �6hat die Arena verlassen!");
		
		if(SoupPvP.killStreak.containsKey(p)){
			SoupPvP.killStreak.remove(p);
		}
	}
	
}
