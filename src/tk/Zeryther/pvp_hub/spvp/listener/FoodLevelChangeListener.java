package tk.Zeryther.pvp_hub.spvp.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

import tk.Zeryther.pvp_hub.spvp.main.SoupPvP;

public class FoodLevelChangeListener implements Listener {
	
	@SuppressWarnings("unused")
	private SoupPvP plugin;
	public FoodLevelChangeListener(SoupPvP plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onFood(FoodLevelChangeEvent e){
		if(e.getEntity() instanceof Player) {
			   e.setFoodLevel(20);
		}
	}
}
