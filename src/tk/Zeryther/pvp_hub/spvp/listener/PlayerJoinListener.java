package tk.Zeryther.pvp_hub.spvp.listener;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import tk.Zeryther.pvp_hub.spvp.main.SoupPvP;
import tk.Zeryther.pvp_hub.spvp.main.Vars;

public class PlayerJoinListener implements Listener {
	
	private SoupPvP plugin;
	public PlayerJoinListener(final SoupPvP plugin){
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onJoin(final PlayerJoinEvent e){
		final Player p = e.getPlayer();
		
		e.setJoinMessage(null);
		
		String world = plugin.getConfig().getString("SPVP.Locations.Lobby.world");
		double x = plugin.getConfig().getDouble("SPVP.Locations.Lobby.x");
		double y = plugin.getConfig().getDouble("SPVP.Locations.Lobby.y");
		double z = plugin.getConfig().getDouble("SPVP.Locations.Lobby.z");
		double yaw = plugin.getConfig().getDouble("SPVP.Locations.Lobby.yaw");
		double pitch = plugin.getConfig().getDouble("SPVP.Locations.Lobby.pitch");
		
		Location login = new Location(Bukkit.getWorld(world), x, y, z);
		login.setYaw((float) yaw);
		login.setPitch((float) pitch);
		
		p.teleport(login);
		
		p.setLevel(0);
		p.setExp(0);
		p.setCanPickupItems(false);
		
		for (PotionEffect effect : e.getPlayer().getActivePotionEffects()) {
			e.getPlayer().removePotionEffect(effect.getType());
		}
		
		if(!(p.isOp() == true) && (p.getGameMode() != GameMode.ADVENTURE)){
			p.setGameMode(GameMode.ADVENTURE);
		}
		
		try {
			plugin.isInDatabase(p.getName());
		} catch (Exception e2) {
			p.sendMessage(Vars.prefix + "�c" + "Something went wrong..");
		   	p.sendMessage(Vars.prefix + "�c" + "Take a look in the console!");
		  	e2.printStackTrace();
		}
		
		/*try {
			plugin.isInDatabase2(p);
		} catch (Exception e2) {
			p.sendMessage(Vars.prefix + "�c" + "Something went wrong..");
		   	p.sendMessage(Vars.prefix + "�c" + "Take a look in the console!");
		  	e2.printStackTrace();
		}*/
		
		p.getInventory().clear();
		p.getInventory().setHelmet(new ItemStack(Material.AIR));
		p.getInventory().setChestplate(new ItemStack(Material.AIR));
		p.getInventory().setLeggings(new ItemStack(Material.AIR));
		p.getInventory().setBoots(new ItemStack(Material.AIR));
		
		try {
			plugin.points.setScore(plugin.getPoints(p.getName()));
		} catch (Exception e1) {
			
		}
		try {
			plugin.kills.setScore(plugin.getKills(p.getName()));
		} catch (Exception e1) {
			
		}
		try {
			plugin.deaths.setScore(plugin.getDeaths(p.getName()));
		} catch (Exception e1) {
			
		}
		try {
			plugin.syncData(p);
		} catch (Exception e1){
			e1.printStackTrace();
		}
		plugin.kills.setScore(100);
		plugin.deaths.setScore(100);
		//p.setScoreboard(plugin.board);
	
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, 
				
				new Runnable(){
			
					public void run(){
						Bukkit.broadcastMessage(Vars.prefix + p.getDisplayName() + " �6hat die Arena betreten!");
					}
			
				}
				
		);
		
		if(plugin.is_ingame.contains(p.getName())){
			plugin.is_ingame.remove(p.getName());
		}
		
		final String mapName = plugin.getConfig().getString("SPVP.MapInfo.name");
		final String builder = plugin.getConfig().getString("SPVP.MapInfo.builder");
		final String link = plugin.getConfig().getString("SPVP.MapInfo.link");
		
		if(SoupPvP.killStreak.containsKey(p)){
			SoupPvP.killStreak.remove(p);
		}
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, 
				
				new Runnable(){
			
					public void run(){
						p.sendMessage("�e[MAP] Name: �b" + mapName);
						p.sendMessage("�e[MAP] Von: �b" + builder);
						p.sendMessage("�e[MAP] Link: �b" + link);
					}
			
				}, 20L
				
		);
	}
	
}
